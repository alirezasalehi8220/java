import java.util.ArrayList;

public class Student {
    private String name;
    private String lastname;
    private String field;
    private long nationalnode;
    private long studentnumber;
    public static ArrayList<Student> students = new ArrayList<>();
    public Student(){

    }
    public Student(String name, String lastname ,String field,long nationalnode,long studentnumber){
        this.name = name;
        this.lastname = lastname;
        this.field =field;
        this.nationalnode = nationalnode;
        this.studentnumber= studentnumber;
    }
    public static Student getByStudentNumber(long studentnumber){
        Student student = null;
        for (int i = 0; i <students.size() ; i++) {
            if(students.get(i).studentnumber == studentnumber){
                student = students.get(i);
                break;
            }
        }
        return student;
    }
    public static void printAll(){
        for (int i = 0; i <students.size() ; i++) {
                print(students.get(i));
        }
    }
    public static void print(Student student){
        System.out.println(
                "name : "+student.name +"\n"
                +"lastname : "+student.lastname +"\n"
                +"field : "+student.field +"\n"
                +"nationalnode : "+student.nationalnode +"\n"
                +"studentnumber : "+student.studentnumber +"\n"
                );
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setField(String field) {
        this.field = field;
    }

    public void setNationalnode(long nationalnode) {
        this.nationalnode = nationalnode;
    }

    public void setStudentnumber(long studentnumber) {
        this.studentnumber = studentnumber;
    }

    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    public String getField() {
        return field;
    }

    public long getNationalnode() {
        return nationalnode;
    }

    public long getStudentnumber() {
        return studentnumber;
    }
}
