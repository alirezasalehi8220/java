public class Main {
    public static void main(String[] args) {
        //create Student 1
        Student student1 = new Student("AliReza","Salehi","computer",1742539,9673149);
        //add to arraylist
        Student.students.add(student1);
        //The second way to create a student
        Student student2 = new Student();
        student2.setName("Ali");
        student2.setLastname("omri");
        student2.setField("it");
        student2.setNationalnode(145545);
        student2.setStudentnumber(9673155);
        Student.students.add(student2);
        Student student = Student.getByStudentNumber(9673149);
        Student.print(student);
        Student.printAll();

    }
}
